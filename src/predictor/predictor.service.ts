import { Injectable } from '@nestjs/common';
import holidays from './data/holiday-config.json';
import schedules from './data/schedule-config.json';
import { Day } from './entities/day';
import { Holiday } from './entities/holiday';
import { Schedule } from './entities/schedule';
import { PredictionResponse } from './types/response.types';

@Injectable()
export class PredictorService {
  /**
   * Predict whether that car can be on the road in base of plate and date
   * @param plate String
   * @param date Date
   */
  async predict(plate: string, date: Date): Promise<PredictionResponse> {
    let canBeOnRoad = false;
    const foundHoliday = await this.getHoliday(date);
    if (!!foundHoliday) {
      return { canBeOnRoad: true, holiday: foundHoliday };
    }
    const scheduleDay = await this.getSchedule(date);
    if (scheduleDay) {
      const lastDigit = +plate.charAt(plate.length - 1);
      if (!!scheduleDay.blockedLicenses.includes(lastDigit)) {
        const foundSchedule = await this.checkScheduleHours(scheduleDay, date);
        if (!foundSchedule) canBeOnRoad = true;
      }
      else
      {
        canBeOnRoad = true;
      }
    }
    return { canBeOnRoad, day: scheduleDay };
  }

  async getHoliday(date: Date): Promise<Holiday> {
    const dateWithoutTime = new Date(date.getTime()).setUTCHours(0, 0, 0, 0);
    const foundHoliday = holidays.find(
      (holiday) =>
        new Date(holiday.date).setUTCHours(0, 0, 0, 0) === dateWithoutTime,
    );
    return foundHoliday ? Object.assign(new Holiday({}), foundHoliday) : null;
  }

  async getSchedule(date: Date): Promise<Day> | null {
    return schedules[date.getDay()] || null;
  }

  async checkScheduleHours(schedule: Day, date: Date): Promise<Schedule> {
    const hour = new Date(`2022-01-01 ${date.getHours()}:${date.getMinutes()}`);
    const foundSchedule = schedule.schedules.find((item) => {
      let start = new Date('2022-01-01 ' + item.startTime);
      let end = new Date('2022-01-01 ' + item.endTime);
      return (
        hour.getTime() >= start.getTime() && end.getTime() >= hour.getTime()
      );
    });
    return foundSchedule ? new Schedule(foundSchedule) : null;
  }
}
