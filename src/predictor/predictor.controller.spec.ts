import { Test, TestingModule } from '@nestjs/testing';
import { PredictorController } from './predictor.controller';
import { PredictorService } from './predictor.service';

describe('PredictorController', () => {
  let controller: PredictorController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PredictorController],
      providers: [PredictorService],
    }).compile();

    controller = module.get<PredictorController>(PredictorController);
  });

  it('should contain canBeOnRoad equal true', async () => {
    const plate = 'IBD3817';
    const date = '2022-11-04 10:00';
    expect(await controller.predict({ plate, date })).toEqual(
      expect.objectContaining({
        canBeOnRoad: true,
      }),
    );
  });
});
