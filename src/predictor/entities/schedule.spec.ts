import { Schedule } from './schedule';

describe('Schedule', () => {
  it('Instance of Schedule', () => {
    expect(
      new Schedule({ startTime: '06:00', endTime: '09:30' }),
    ).toBeInstanceOf(Schedule);
  });
});
