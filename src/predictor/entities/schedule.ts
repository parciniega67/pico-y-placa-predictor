export class Schedule {
    constructor(partial: Partial<Schedule>) {
        Object.assign(this, partial);
    }
    startTime: string;
    endTime: string;
}
