export class Holiday {
    constructor(partial: Partial<Holiday>) {
        Object.assign(this, partial);
    }
    name: string;
    date: Date;
}
