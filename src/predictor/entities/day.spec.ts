import { Day } from './day';

describe('Day', () => {
  it('Instance of Day', () => {
    expect(new Day({name: 'Lunes', blockedLicenses:[1,2]})).toBeInstanceOf(Day);
  });
});
