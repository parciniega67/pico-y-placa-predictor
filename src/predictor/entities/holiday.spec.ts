import { Holiday } from './holiday';

describe('Holiday', () => {
  it('Instance of Holiday', () => {
    expect(new Holiday({})).toBeInstanceOf(Holiday);
  });
});
