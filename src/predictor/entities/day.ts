import { Schedule } from './schedule';

export class Day {
  constructor(partial: Partial<Day>) {
    Object.assign(this, partial);
  }

  name: string;
  schedules: Schedule[];
  blockedLicenses: number[];
}
