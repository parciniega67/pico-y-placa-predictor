import { Test, TestingModule } from '@nestjs/testing';
import { PredictorService } from './predictor.service';
import { Day } from './entities/day';

describe('Predictor Service', () => {
  let service: PredictorService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PredictorService],
    }).compile();

    service = module.get<PredictorService>(PredictorService);
  });

  it('should be holiday', async () => {
    const date = '2022-11-04 10:00';
    const foundHoliday = { date: '2022-11-04', name: 'Día de Difuntos' };
    expect(await service.getHoliday(new Date(date))).toEqual(foundHoliday);
  });

  it('should be monday schedule', async () => {
    const date = '2022-10-17 10:00';
    const mondaySchedule = new Day({
      name: 'Lunes',
      schedules: [
        { startTime: '06:00', endTime: '09:30' },
        { startTime: '16:00', endTime: '21:00' },
      ],
      blockedLicenses: [1, 2],
    });
    expect(await service.getSchedule(new Date(date))).toEqual(mondaySchedule);
  });

  it('should be between hours', async () => {
    const date = '2022-10-17 07:00';
    const foundSchedule = { endTime: '09:30', startTime: '06:00' };
    const mondaySchedule = new Day({
      name: 'Lunes',
      schedules: [
        { startTime: '06:00', endTime: '09:30' },
        { startTime: '16:00', endTime: '21:00' },
      ],
      blockedLicenses: [1, 2],
    });
    expect(
      await service.checkScheduleHours(mondaySchedule, new Date(date)),
    ).toEqual(foundSchedule);
  });

  it('handle holiday', async () => {
    const plate = 'IBD3817';
    const date = '2022-11-04 10:00';
    const foundHoliday = { date: '2022-11-04', name: 'Día de Difuntos' };
    expect((await service.predict(plate, new Date(date))).holiday).toEqual(
      foundHoliday,
    );
  });

  it('handle not available hours', async () => {
    const plate = 'IBD3812';
    const date = '2022-10-17 09:00';
    expect(await service.predict(plate, new Date(date))).toEqual(
      expect.objectContaining({ canBeOnRoad: false }),
    );
  });

  it('handle available hours', async () => {
    const plate = 'IBD3812';
    const date = '2022-10-17 11:00';
    expect(await service.predict(plate, new Date(date))).toEqual(
        expect.objectContaining({ canBeOnRoad: true }),
    );
  });


  it('handle available days', async () => {
    const plate = 'IBD3813';
    const date = '2022-10-17 11:00';
    expect(await service.predict(plate, new Date(date))).toEqual(
        expect.objectContaining({ canBeOnRoad: true }),
    );
  });
});
