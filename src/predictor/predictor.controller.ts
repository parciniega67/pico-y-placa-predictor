import {
  Body,
  Controller,
  HttpCode,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { PredictorService } from './predictor.service';
import { PredictionRequestDto } from './dto/predictionRequest.dto';

@Controller('predictor')
export class PredictorController {
  constructor(private predictorService: PredictorService) {}

  @Post('/')
  @HttpCode(200)
  @UsePipes(
    new ValidationPipe({
      enableDebugMessages: true,
      transform: true,
      skipMissingProperties: false,
      whitelist: true,
      forbidNonWhitelisted: true,
      forbidUnknownValues: true,
      errorHttpStatusCode: 422,
    }),
  )
  async predict(@Body() req: PredictionRequestDto) {
    return this.predictorService.predict(req.plate, new Date(req.date));
  }
}
