import { Day } from '../entities/day';
import { Holiday } from '../entities/holiday';

export interface PredictionResponse {
  day?: Day;
  holiday?: Holiday;
  canBeOnRoad: boolean;
}
