import {IsAlphanumeric, IsNotEmpty, IsDateString} from 'class-validator';

export class PredictionRequestDto {
  @IsNotEmpty()
  @IsAlphanumeric()
  plate: string;

  @IsNotEmpty()
  @IsDateString()
  date: string;
}
