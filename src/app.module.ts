import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { PredictorModule } from './predictor/predictor.module';

@Module({
  imports: [ConfigModule.forRoot(), PredictorModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
